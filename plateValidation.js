

function isValidPlate(plate) { // eslint-disable-line no-unused-vars
  var re = /(\d\d\d\d[BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ])/i;
  return plate.match(re) != null;
}

